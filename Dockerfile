FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

ARG VERSION=2.0.0-beta.1
RUN curl -L https://github.com/tldraw/tldraw/archive/refs/tags/v${VERSION}.tar.gz | \
    tar -zxvf - --strip-components=1

RUN yarn install && rm -rf /usr/local/share/.cache/yarn

# cannot rename dir because it complains with "Duplicate workspace name"
RUN mv /app/code/packages/assets /app/pkg/assets && \
    ln -sf /run/tldraw/assets /app/code/packages/assets

RUN rm -rf /app/code/.lazy && \
    ln -sf /run/tldraw/.lazy /app/code/.lazy && \
    ln -sf /run/tldraw/apps/examples/.lazy /app/code/apps/examples/.lazy && \
    mkdir -p /app/code/apps/examples/node_modules && \
    ln -sf /run/tldraw/apps/examples/node_modules/.vite /app/code/apps/examples/node_modules/.vite && \
    node /app/code/packages/tldraw/scripts/copy-css-files.mjs && \
    mv /app/code/apps/examples/src/index.tsx /app/code/apps/examples/src/index.orig.tsx

COPY cloudron-index.tsx /app/code/apps/examples/src/index.tsx
COPY vite.config.ts /app/code/apps/examples/vite.config.ts

# fix window title
RUN sed -e 's/tldraw examples/tldraw/' -i /app/code/apps/examples/src/index.html

COPY start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
