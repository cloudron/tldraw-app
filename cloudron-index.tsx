import { getAssetUrlsByMetaUrl } from '@tldraw/assets/urls'
import {
        DefaultErrorFallback,
        ErrorBoundary,
        setDefaultEditorAssetUrls,
        setDefaultUiAssetUrls,
} from '@tldraw/tldraw'
import { StrictMode } from 'react'
import { createRoot } from 'react-dom/client'
import { RouterProvider, createBrowserRouter } from 'react-router-dom'

import ExamplesTldrawLogo from './components/ExamplesTldrawLogo'
import { ListLink } from './components/ListLink'

import BasicExample from './BasicExample'

const assetUrls = getAssetUrlsByMetaUrl()
setDefaultEditorAssetUrls(assetUrls)
setDefaultUiAssetUrls(assetUrls)

const rootElement = document.getElementById('root')
const root = createRoot(rootElement!)

root.render(
    <StrictMode>
        <BasicExample />
    </StrictMode>
)
