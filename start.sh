#!/bin/bash

set -eu

mkdir -p /run/tldraw/.lazy /run/tldraw/apps/examples/.lazy /run/tldraw/apps/examples/node_modules/.vite

if [[ ! -d /run/tldraw/assets ]]; then
	cp -R /app/pkg/assets /run/tldraw/assets
fi

echo "==> Fixing permissions"
chown -R cloudron:cloudron /run/tldraw
chown -R cloudron:cloudron /app/data

echo "==> Running TLDraw"
cd /app/code/apps/examples/
exec gosu cloudron:cloudron /app/code/node_modules/.bin/vite --host
