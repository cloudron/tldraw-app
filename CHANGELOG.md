[0.1.0]
* Initial version for TLDraw 1.7.24

[0.2.0]
* optionalSSo flag

[0.3.0]
* Increase default memory limit

[1.0.0]
* Update TLDraw to 1.28.0

[2.0.0]
* Updated to TLDraw 2.0.0-alpha.17

[2.0.1]
* Directly use vitejs instead of half broken lazyrepo command

[2.0.2]
* Update TLDraw to 2.0.0-alpha.18

[2.0.3]
* Update TLDraw to 2.0.0-alpha.19

[2.1.0]
* Update TLDraw to 2.0.0-beta.1
* [Full changelog](https://github.com/tldraw/tldraw/releases/tag/v2.0.0-beta.1)
* add speech bubble example (#2362)
* Fix clicking off the context menu (#2355)
* fix read only page menu (#2356)
* refactor: Keep hook function convention the same (#2358)
* focus on container before deleting to avoid losing focus (#2354)
* Stop shape text labels being hoverable when context menu is open (#2352)

